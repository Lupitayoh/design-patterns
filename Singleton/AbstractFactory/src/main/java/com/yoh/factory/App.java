/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh.factory;

import com.yoh.factory.inter.FabricaAbstracta;
import com.yoh.factory.inter.IConexion;
import com.yoh.factory.inter.IConexionRest;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){
        FabricaAbstracta fabricaBD = FabricaProductor.getFactory("BD");
        IConexion cxBD1 = fabricaBD.getBD("MYSQL");

        cxBD1.conectar();

        System.out.println("DUDE");
        FabricaAbstracta fabricaREST = FabricaProductor.getFactory("REST");
        IConexionRest cxRS1 = fabricaREST.getRest("COMPRAS");

        cxRS1.leerUrl("https://www.youtube.com/subscription_center?add_user=mitocode");
    }
    
    
}
