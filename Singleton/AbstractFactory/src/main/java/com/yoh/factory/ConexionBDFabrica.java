package com.yoh.factory;

import com.yoh.factory.inter.FabricaAbstracta;
import com.yoh.factory.inter.IConexion;
import com.yoh.factory.inter.IConexionRest;
import com.yoh.factory.inter.impl.*;

public class ConexionBDFabrica implements FabricaAbstracta {

    @Override
    public IConexion getBD(String motor) {
        if (motor == null) {
            return new ConexionVacia();
        }
        if (motor.equalsIgnoreCase("MYSQL")) {
            return new ConexionMySql();
        } else if(motor.equalsIgnoreCase("ORACLE")) {
            return new ConexionOracle();
        } else if(motor.equalsIgnoreCase("POSTGRE")) {
            return new ConexionPostgreSQL();
        }else  if(motor.equalsIgnoreCase("SQL")) {
            return new ConexionSQLServer();
        }

        return new ConexionVacia();
    }

    @Override
    public IConexionRest getRest(String area) {
        return null;
    }
}
