package com.yoh.factory;

import com.yoh.factory.inter.FabricaAbstracta;
import com.yoh.factory.inter.IConexion;
import com.yoh.factory.inter.IConexionRest;
import com.yoh.factory.inter.impl.*;

public class ConexionRESTFabrica implements FabricaAbstracta{


    @Override
    public IConexion getBD(String motor) {
        return null;
    }

    @Override
    public IConexionRest getRest(String area) {
        System.out.println("impl getREst");
        if (area == null) {
            return new ConexionRestNoArea();
        }
        if(area.equalsIgnoreCase("COMPRAS")) {
            return new ConexionRestCompras();
        } else if (area.equalsIgnoreCase("VENTAS")) {
            return new ConexionRestVentas();
        }
        return new ConexionRestNoArea();
    }
}
