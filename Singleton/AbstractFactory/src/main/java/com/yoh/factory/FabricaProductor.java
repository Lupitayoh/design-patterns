package com.yoh.factory;
import com.yoh.factory.ConexionBDFabrica;
import com.yoh.factory.inter.FabricaAbstracta;

public class FabricaProductor {

    public static FabricaAbstracta getFactory(String tipoFabrica) {
        if(tipoFabrica. equalsIgnoreCase("BD")) {
            return new ConexionBDFabrica();
        }else if(tipoFabrica.equalsIgnoreCase("REST")) {
            return new ConexionRESTFabrica();
        }
        return null;
    }
}