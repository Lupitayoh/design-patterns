package com.yoh.factory.inter;

public interface FabricaAbstracta {

    IConexion getBD(String motor);
    IConexionRest getRest(String area);
}
