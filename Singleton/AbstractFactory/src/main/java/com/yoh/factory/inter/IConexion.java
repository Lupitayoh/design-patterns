package com.yoh.factory.inter;

public interface IConexion {

    public void conectar();
    public void desconectar();
}
