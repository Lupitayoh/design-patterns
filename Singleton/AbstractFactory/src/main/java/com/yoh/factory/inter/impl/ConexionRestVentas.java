package com.yoh.factory.inter.impl;

import com.yoh.factory.inter.IConexionRest;

public class ConexionRestVentas implements IConexionRest {

    @Override
    public void leerUrl(String url) {
        System.out.println("Conectándose a " + url);
    }
}
