/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh;

import com.yoh.command.Cuenta;
import com.yoh.command.DepositarImpl;
import com.yoh.command.Invoker;
import com.yoh.command.RetirarImpl;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){

       Cuenta cuenta = new Cuenta(1,  100);

        DepositarImpl opDepositar = new DepositarImpl(cuenta, 100);
        RetirarImpl opRetirar = new RetirarImpl(cuenta, 50);

        Invoker ivk = new Invoker();
        ivk.recibirOperacion(opDepositar);
        ivk.recibirOperacion(opRetirar);

        ivk.realizarOperaciones();

    }
    
    
}
