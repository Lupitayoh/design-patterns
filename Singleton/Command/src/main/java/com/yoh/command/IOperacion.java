package com.yoh.command;

@FunctionalInterface //Gracias a Java 8 puedo denotar que esta es una interfaz funcional debido a que solo tiene un metodo definido
public interface IOperacion {

    void execute();
}
