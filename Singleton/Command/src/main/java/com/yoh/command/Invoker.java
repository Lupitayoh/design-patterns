package com.yoh.command;

import java.util.ArrayList;
import java.util.List;

public class Invoker {

    private List<IOperacion> operaciones = new ArrayList<>();

    public void recibirOperacion(IOperacion operacion) {
        this.operaciones.add(operacion);
    }

    public void realizarOperaciones() {
        this.operaciones.forEach(x -> x.execute());//iteracion de operaciones y por cada operacion encontrada se ejecuta
        this.operaciones.clear();
    }
}
