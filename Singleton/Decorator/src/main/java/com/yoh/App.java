/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh;

import com.yoh.decorator.BlindajeDecorador;
import com.yoh.inter.ICuentaBancaria;
import com.yoh.inter.impl.CuentaAhorro;
import com.yoh.model.Cuenta;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){

        Cuenta c = new Cuenta(1, "MitoCode");

        ICuentaBancaria cuenta = new CuentaAhorro();
        ICuentaBancaria cuentaBlindada = new BlindajeDecorador(cuenta);

        cuentaBlindada.abrirCuenta(c);
    }
    
    
}
