package com.yoh.inter;

import com.yoh.model.Cuenta;

public interface ICuentaBancaria {
    //representa dos tipos de cuenta
    public void abrirCuenta(Cuenta c);
}
