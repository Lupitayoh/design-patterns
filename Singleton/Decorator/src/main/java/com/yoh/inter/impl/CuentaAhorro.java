package com.yoh.inter.impl;

import com.yoh.inter.ICuentaBancaria;
import com.yoh.model.Cuenta;

public class CuentaAhorro implements ICuentaBancaria {

    @Override
    public void abrirCuenta(Cuenta c){
        System.out.println("---------------------------");
        System.out.println("Se abri[o una cuenta de Ahorros");
        System.out.println("Cliente: " + c.getCliente());
    }
}
