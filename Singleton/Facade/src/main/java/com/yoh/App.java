/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh;

import com.yoh.facade.CheckFacade;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){
       CheckFacade cliente1 = new CheckFacade();
       cliente1.buscar("02/07/2018", "08/07/2018", "Lima", "Cancun");

        CheckFacade cliente2 = new CheckFacade();
        cliente2.buscar("02/07/2018", "08/07/2018", "Lima", "Quito");
    }
    
    
}
