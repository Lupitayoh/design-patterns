/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh.factory;

import com.yoh.factory.inter.IConexion;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){
        ConexionFabrica fabrica = new ConexionFabrica();

        IConexion cx1 = fabrica.getConexion("ORACLE");
        cx1.conectar();
        cx1.desconectar();

        IConexion cx2 = fabrica.getConexion("MYSQL");
        cx2.conectar();
        cx2.desconectar();

        IConexion cx3 = fabrica.getConexion("SQL");
        cx3.conectar();
        cx3.desconectar();

        IConexion cx4 = fabrica.getConexion("fsdfs");
        cx4.conectar();
        cx4.desconectar();
    }
    
    
}
