package com.yoh.factory;

import com.yoh.factory.inter.IConexion;
import com.yoh.factory.inter.impl.*;

public class ConexionFabrica {

    public IConexion getConexion(String motor) {
        if(motor == null) {
            return new ConexionVacia();
        }
        if(motor.equalsIgnoreCase("MYSQL")){
            return new ConexionMySql();
        }
        if(motor.equalsIgnoreCase("ORACLE")){
            return new ConexionOracle();
        }
        if(motor.equalsIgnoreCase("POSTGRES")){
            return new ConexionPostgreSQL();
        }
        if(motor.equalsIgnoreCase("SQL")){
            return new ConexionSQLServer();
        }
        return new ConexionVacia();
    }
}
