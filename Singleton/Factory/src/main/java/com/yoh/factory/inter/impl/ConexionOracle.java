package com.yoh.factory.inter.impl;

import com.yoh.factory.inter.IConexion;

public class ConexionOracle implements IConexion{

    private String host;
    private String puerto;
    private String usuario;
    private String contrasena;

    public ConexionOracle() {
        this.host = "localhost";
        this.puerto = "3306";
        this.usuario = "oracle";
        this.contrasena = "123";
    }

    @Override
    public void conectar() {
        System.out.println("Aqui se conectó Oracle");
    }

    @Override
    public void desconectar() {
        System.out.println("Aquí se desconectó Oracle");
    }
}
