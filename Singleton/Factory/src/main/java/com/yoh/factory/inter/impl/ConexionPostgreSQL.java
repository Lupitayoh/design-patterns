package com.yoh.factory.inter.impl;

import com.yoh.factory.inter.IConexion;

public class ConexionPostgreSQL implements IConexion{
    private String host;
    private String puerto;
    private String usuario;
    private String contrasena;

    public ConexionPostgreSQL() {
        this.host = "localhost";
        this.puerto = "3306";
        this.usuario = "root";
        this.contrasena = "123";
    }

    @Override
    public void conectar() {
        System.out.println("Aqui se conectó Postgre");
    }

    @Override
    public void desconectar() {
        System.out.println("Aquí se desconectó Posgre");
    }
}
