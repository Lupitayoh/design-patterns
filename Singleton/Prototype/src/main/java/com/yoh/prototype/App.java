/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh.prototype;


import com.yoh.prototype.model.CuentaAHImpl;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){
        CuentaAHImpl cuentaAhorro = new CuentaAHImpl();
        cuentaAhorro.setMonto(200);

        CuentaAHImpl cuentaAhorro2 = new CuentaAHImpl();

        CuentaAHImpl cuentaClonada = (CuentaAHImpl) cuentaAhorro.clonar();

        System.out.println(cuentaAhorro);
        System.out.println(cuentaAhorro2);
        System.out.println(cuentaClonada);
        if(cuentaClonada != null) {
            System.out.println(cuentaClonada);
        }
        System.out.println(cuentaClonada == cuentaAhorro);
    }
    
    
}
