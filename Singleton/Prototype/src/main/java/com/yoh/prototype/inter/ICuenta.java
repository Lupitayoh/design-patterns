package com.yoh.prototype.inter;

public interface ICuenta extends Cloneable{

    ICuenta clonar();
}
