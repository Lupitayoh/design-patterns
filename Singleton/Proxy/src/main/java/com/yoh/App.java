/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh;

import com.yoh.inter.ICuenta;
import com.yoh.model.Cuenta;
import com.yoh.proxy.CuentaProxy;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){

       Cuenta c = new Cuenta(1, "mitocode", 100);

        ICuenta cuentaProxy = new CuentaProxy();
        cuentaProxy.mostrarSaldo(c);
        c = cuentaProxy.depositarDinero(c, 50);
        c = cuentaProxy.retirarDinero(c, 20);
        cuentaProxy.mostrarSaldo(c);
    }
    
    
}
