/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh.singleton;

/**
 *
 * @author Lupitayoh
 */
public class App {
    
    public static void main(String arg[]){
        //Instanciacion por constructor prohibido por ser privado
        //Conexion c= new Conexion();
        Conexion c = Conexion.getInstancia();
        c.conectar();
        c.desconectar();
        
        boolean rpta = c instanceof Conexion;
        System.out.println("rpta");
    }
    
    
}
