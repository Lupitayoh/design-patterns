/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoh.singleton;

/**
 *
 * @author Lupitayoh
 */
public class Conexion {
    
    //Declaraci[on
    private static Conexion instancia;
    //private static Conexion instancia = new Conexion();
    
    //Para evitar instancia mediante operador new
    private Conexion(){
    
    }
    
    //Para obtener la instancia unicamente por este metodo
    //notese la palabra resevada static hace posible el acceso mediante Clase.metodo
    public static Conexion getInstancia() {
        if(instancia == null){
            instancia = new Conexion();
        }
        return instancia;
    }
    
    //Metodo de prueba
    public void conectar(){
        System.out.println("Me conecte a la BD");
    }
    
    //Metodo de prueba
    public void desconectar() {
        System.out.println("Me desconecte de BD");
    }
}
